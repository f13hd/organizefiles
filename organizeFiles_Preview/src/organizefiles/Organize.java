package organizefiles;

import java.io.File;
import java.io.FileFilter;
import javax.swing.JOptionPane;
import org.apache.commons.io.FilenameUtils;
/*

 
 problem :
 I have many files different types into one folder .
 i want to make a programe to organize all files.
   
 steps :
 - collect most of extenstion of files .=> mp4,mp3,flv & jpg,jpeg,ico,png & docx,pdf ....etc
 - create jfilechoose to select path of folder to organize them into folders.
 - move files into specific folder . images , documents.
 
 By : @F13hd
 website : f13hd.blogspot.com
Email : alking@hotmail.de

 updates :
 - no overwrite folder . u can add files to old folder if exist
 - disable execute Button if not selected path of folder
 - if exist file in destination . rename or override
 */

public class Organize {

    private final String[] image = {"jpeg", "jpg", "png", "gif", "bmp", "ico"};
    private final String[] video = {"mp4", "flv", "mp3", "wmv", "3gp", "avi"};
    private final String[] compress = {"zip", "tar", "gz"};
    private final String[] document = {"docx", "doc", "pdf", "ppt", "xls", "txt", "odt", "ott"};
    private final String[] Programs = {"exe", "sh"};

    private String path;
    private String separator;

    public String getSeparator() 
    {
        return separator;
    }

    public void setSeparator(String separator) 
    {
        this.separator = separator;
    }

    public void setPath(String path) 
    {
        this.path = path;
    }

    public String getPath() 
    {
        return path;
    }
     

    public void checkPictures()
    {
        organize("Images", image);
        MainWindow.StatusArea.append("finished from images folder \n");
    }

    public void checkVideos()
    {
        organize("Videos", video);
        MainWindow.StatusArea.append("finished from videos folder \n");
    }

    public void checkDocuments()
    {
        organize("Documents", document);
        MainWindow.StatusArea.append("finished from documents folder \n");
    }

    public void checkCompress() 
    {
        organize("Compress", compress);
        MainWindow.StatusArea.append("finished from compress folder \n");
    }

    public void checkPrograms() 
    {
        organize("Programs", Programs);
        MainWindow.StatusArea.append("finished from Programs folder \n");
    }

    public void checkAll() {
        organize("Images", image);
        MainWindow.StatusArea.append("finished from images folder \n");
        organize("Videos", video);
        MainWindow.StatusArea.append("finished from videos folder \n");
        organize("Documents", document);
        MainWindow.StatusArea.append("finished from documents folder \n");
        organize("Compress", compress);
        MainWindow.StatusArea.append("finished from compress folder \n");
    }
//        

    private void organize(String nameFoloder, String[] ext) {
        // path is ready
        // make folders then move files into it 
        // check if foldre is exist or not => Files.exists(dir,LinkOption.NOFOLLOW_LINKS)
        // if exist create two options : overwrite & enter new name of folder by Joptionpane   

        try {

            // is exist or not 
            // create new folder with name => namefolder
            File folder = new File(path + nameFoloder + separator);

            if (!folder.exists()) { // not exist ..create new folder 
                System.out.println("is not exists folder... will create it  ");

                if (folder.mkdir()) {
                    System.out.println("is created in path =>" + folder.getPath());
                    moveFiles(folder, ext);
                }

            } else { // is exist . add to old Folder or create  new one 

                System.out.println("folder is exist on path =>" + folder.getPath());

                int ch = JOptionPane.showConfirmDialog(null, "add to " + folder.getName() + " Folder :YES & New Folder : NO", "", JOptionPane.YES_NO_OPTION);

                switch (ch) {
                    case JOptionPane.YES_OPTION:

                        moveFiles(folder, ext);
                        System.out.println("all files added to " + folder.getName());
                        break;

                    case JOptionPane.NO_OPTION:

                        // rename a new folder then create it 
                        String str = JOptionPane.showInputDialog("please enter a folder name :");
                        folder = new File(path + str + separator);

                        if (folder.mkdir()) {

                            System.out.println("created a folder with path =>" + folder.getPath());
                            moveFiles(folder, ext);

                        } else {
                            System.out.println("can't create new Folder");
                        }

                        break;

                    case JOptionPane.CLOSED_OPTION: // when close window => red [x]
                        System.out.println("close window");

                        break;

                    default:
                        System.err.println("no choose option");
                        break;
                } // switch closed

            }

        } catch (Exception x) {
            System.err.println(x);
        }

    }

    private void moveFiles(File Folder, final String[] extension) {

        File parent = new File(Folder.getParent());

        FileFilter filter = prepareFileFilter(Folder, extension);

        File[] listFiles = parent.listFiles(filter);

        String newPath;
        for (File oldFile : listFiles) {

            newPath = Folder.getPath() + separator + oldFile.getName();

            File newFile = new File(newPath);//new path
            if (!newFile.exists()) {
                System.out.println(newFile.getPath());
                oldFile.renameTo(newFile); // move to new distenation
            } else {
                int ch = JOptionPane.showConfirmDialog(null, newFile.getName() + " Already Exist \n Rename : YES , Override : No", "", JOptionPane.YES_NO_OPTION);
              
                switch (ch) { // when has same name file in folder . override or change name
                    case JOptionPane.YES_OPTION: // rename

                        String newName = JOptionPane.showInputDialog(null, "Enter file name");
                        newName += "." + FilenameUtils.getExtension(newFile.getName());
                        File newNamedFile = new File(newFile.getParent() + separator + newName);
                        System.out.println(newNamedFile.getPath());
                        oldFile.renameTo(newNamedFile);

                        break;

                    case JOptionPane.NO_OPTION: // move and override

                        oldFile.renameTo(newFile);

                        break;

                    default:
                        System.out.println("No Choose ");
                        break;
                } // end switch

            } // end else
        } //end for

    } // end moveFile method

    public static FileFilter prepareFileFilter(File Folder, final String[] ex) {
        FileFilter filter = new FileFilter() {

            @Override
            public boolean accept(File pathname) {
                for (String ext : ex) {

                    if (ext.equals(FilenameUtils.getExtension(pathname.getPath()))) {
                        return true;
                    }

                }
                return false;
            }
        };
        return filter;
    }

}
